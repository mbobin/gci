# frozen_string_literal: true

require 'active_support/all'
require 'yaml'
require 'gci/version'
require 'gci/cli'
require 'gci/job'
require 'gci/pipeline'

module GCI
  class Error < StandardError; end

  def self.pipeline(&block)
    if @pipeline
      yield(@pipeline) if block_given?
    else
      @pipeline = Pipeline.new(&block)
    end

    @pipeline
  end

  def self.root_pipeline
    @root_pipeline ||= Pipeline.new do |pipeline|
      pipeline.gitlab_ci_file = '.gitlab-ci.yml'
      pipeline.image = 'ruby:alpine'
      pipeline.stages = %i[build run]

      pipeline.jobs.build(name: 'generate-config') do |job|
        job.stage = :build
        job.before_script = [
          'gem install gci'
        ]

        job.script = [
          'gci --config .gitlab-ci.rb generate'
        ]

        job.artifacts = {
          paths: ['child.gitlab-ci.yml']
        }
      end

      pipeline.jobs.build(name: 'execute-config') do |job|
        job.stage = :run
        job.trigger = {
          include: [
            artifact: 'child.gitlab-ci.yml',
            job: 'generate-config'
          ],
          strategy: 'depend'
        }
      end
    end

    yield(@root_pipeline) if block_given?
    @root_pipeline
  end
end
