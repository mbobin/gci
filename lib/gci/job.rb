# frozen_string_literal: true

module GCI
  class Job
    class Collection
      def initialize
        @data = []
      end

      def build(**attrs, &block)
        job = Job.new(**attrs, &block)
        @data << job
        job
      end

      def <<(job)
        @data << job

        self
      end

      def to_h
        @data.each_with_object({}) do |job, acc|
          acc.merge!(job.name => job.attributes)
          acc
        end
      end

      def [](name)
        @data.detect { |job| job.name == name }
      end
    end

    def self.define_attr_accessors(name)
      define_method("#{name}=") do |value|
        @attrs[name] = value
      end

      define_method(name) do
        @attrs[name]
      end
    end

    define_attr_accessors :name
    define_attr_accessors :script
    define_attr_accessors :after_script
    define_attr_accessors :allow_failure
    define_attr_accessors :artifacts
    define_attr_accessors :before_script
    define_attr_accessors :cache
    define_attr_accessors :coverage
    define_attr_accessors :dependencies
    define_attr_accessors :environment
    define_attr_accessors :except
    define_attr_accessors :extends
    define_attr_accessors :image
    define_attr_accessors :include
    define_attr_accessors :interruptible
    define_attr_accessors :only
    define_attr_accessors :pages
    define_attr_accessors :parallel
    define_attr_accessors :release
    define_attr_accessors :resource_group
    define_attr_accessors :retry
    define_attr_accessors :rules
    define_attr_accessors :services
    define_attr_accessors :stage
    define_attr_accessors :tags
    define_attr_accessors :timeout
    define_attr_accessors :trigger
    define_attr_accessors :variables
    define_attr_accessors :when

    def initialize(**attrs)
      @attrs = attrs
      yield(self) if block_given?
    end

    def stage=(value)
      @attrs[:stage] = value.to_s
    end

    def change(&block)
      self.class.new(**@attrs.deep_dup, &block)
    end

    def to_h
      data = @attrs
      data[:stage] = data[:stage].to_s if data[:stage]
      data
    end

    def attributes
      to_h.except(:name)
    end
  end
end
