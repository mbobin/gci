# frozen_string_literal: true

module GCI
  class CLI
    COMMANDS = %i[generate root].freeze

    def dispatch(command, options)
      raise Error, "Invalid command '#{command}'" unless COMMANDS.include?(command.to_sym)

      public_send(command, options)
    end

    def generate(options)
      evaluate_user_config(options)

      GCI.pipeline.write
    end

    def root(options)
      evaluate_user_config(options)

      GCI.root_pipeline.write
    end

    private

    def evaluate_user_config(options)
      raise Error, 'Missing configuration file' unless options[:config].present?

      require(Pathname(options[:config]).expand_path)
    end
  end
end
