# frozen_string_literal: true

module GCI
  class Pipeline
    attr_reader :jobs
    attr_accessor :stages, :image, :variables, :workflow
    attr_writer :gitlab_ci_file

    def initialize
      @jobs = Job::Collection.new
      @stages = %i[build test deploy]
      @gitlab_ci_file = 'child.gitlab-ci.yml'

      yield(self) if block_given?
    end

    def write
      File.open(@gitlab_ci_file, 'wb') do |file|
        file.write(to_yaml)
      end
    end

    def to_h
      data = {}
      data.merge!(stages: stages.map(&:to_s))
      data.merge!(image: @image) if @image.present?
      data.merge!(workflow: @workflow) if @workflow.present?
      data.merge!(@jobs.to_h)
      data
    end

    def to_yaml
      to_h.to_yaml
    end
  end
end
