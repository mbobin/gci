# frozen_string_literal: true

RSpec.describe GCI do
  it 'has a version number' do
    expect(GCI::VERSION).not_to be nil
  end
end
