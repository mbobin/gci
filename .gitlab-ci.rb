# frozen_string_literal: true

GCI.root_pipeline do |pipeline|
  pipeline
    .jobs["generate-config"]
    .before_script
    .unshift("ruby -v")
end

GCI.pipeline do |pipeline|
  pipeline.image = 'ruby:alpine'

  test_job = GCI::Job.new(stage: :test) do |job|
    job.before_script = [
      'apk add --update git',
      "bundle config set path 'vendor/ruby'",
      'bundle install'
    ]
    job.cache = {
      key: { files: ['Gemfile.lock'] },
      paths: ['vendor/ruby']
    }
  end

  {
    rspec: nil,
    rubocop: '--config .rubocop.yml'
  }.each do |job_name, options|
    test_job.change do |job|
      job.name = job_name
      job.script = ["bundle exec #{job_name} #{options}"]
      pipeline.jobs << job
    end
  end

  puts pipeline.to_yaml
end
